git clone https://gitlab.com/kevinprince/ansible.git

cd ansible 

curl http://192.168.1.10/ansible/users.yml > group_vars/all/users.yml

ansible-galaxy install -r requirements.yml

ansible-playbook users.yml

rm -rf ansible