curl https://gitlab.com/kevinprince/ansible/raw/master/bootstrap.sh | bash

sudo apt-get -y install ca-certificates
cd /tmp && wget https://apt.puppetlabs.com/puppet5-release-bionic.deb
sudo dpkg -i /tmp/puppet5-release-bionic.deb
echo "deb http://deb.theforeman.org/ bionic 1.19" | sudo tee /etc/apt/sources.list.d/foreman.list
echo "deb http://deb.theforeman.org/ plugins 1.19" | sudo tee -a /etc/apt/sources.list.d/foreman.list
sudo apt-get -y install ca-certificates
wget -q https://deb.theforeman.org/pubkey.gpg -O- | sudo apt-key add -
sudo apt-get update && sudo apt-get -y install foreman-installer
