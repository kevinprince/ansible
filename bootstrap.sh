#!/bin/bash
sudo su
apt-get update
apt-get -y upgrade
apt-get -y install \
    autoconf \
    apt-transport-https \
    bison \
    build-essential \
    ca-certificates \
    curl \
    git \
    gnupg2 \
    libssl-dev \
    libyaml-dev \
    libreadline-dev \
    zlib1g-dev \
    libncurses5-dev \
    libffi-dev \
    libgdbm-dev \
    libxss1 \
    libasound2 \
    nano \
    net-tools \
    open-vm-tools \
    python \
    python-dev \
    ruby \
    ruby-dev \
    software-properties-common \
    unzip \
    wget

#Install Python
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
rm get-pip.py

# Install tools
pip install ansible awscli

#copy and run ansible
git clone https://gitlab.com/kevinprince/ansible.git

cd ansible 

curl http://192.168.1.10/ansible/users.yml > group_vars/all/users.yml
curl http://192.168.1.10/ansible/vars.yml > group_vars/all/vars.yml

ansible-galaxy install -r requirements.yml

ansible-playbook bootstrap.yml

rm -rf ansible